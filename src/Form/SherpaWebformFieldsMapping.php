<?php

namespace Drupal\sherpa_webform\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SherpaWebformFieldsMapping
 *
 * @package Drupal\sherpa_webform\Form
 */
class SherpaWebformFieldsMapping extends FormBase{

  /**
   * {@inheritdoc}
   */	
  public function getFormId() {
    return 'sherpa_webform_fields_mapping';
  }

  /**
   * {@inheritdoc}
   */  
  public function buildForm(array $form, FormStateInterface $form_state) {

    $webform = \Drupal::entityTypeManager()->getStorage('webform')
      ->load(\Drupal::request()->get('webform'));
    $elements = Yaml::decode($webform->get('elements'));

    $form_state->setStorage(['webformId' => $webform->id(), 'elements' => $elements]);

    $default_values = $this->config('sherpa_webform.webform_field_mapping.' . $webform->id())->getRawData();
	
    $sherpa_manager = \Drupal::service('sherpa_webform.sherpa_manager');
    $tested_fields = $sherpa_manager->get_fields();

    // Get the default form values.
	$custom_form_values = $this->config('sherpa_webform.webform_form_control.' . $webform->id());
    $sherpa_global_values = $this->config('sherpa_webform.settings');

    $overwrite_community_id = $custom_form_values->get('overwrite_community_id');
    $custom_community_id = $custom_form_values->get('custom_community_id');
    $default_community_id = $sherpa_global_values->get('community_id');
    $default_use_quickset = $sherpa_global_values->get('use_quickset');
    if ($overwrite_community_id === 1 && isset($custom_community_id)) {
      $temp_community_id = $custom_community_id;
    } else {
      $temp_community_id = $default_community_id;
    }

    $form['attach_form'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send this form to Sherpa'),
      '#default_value' => $custom_form_values->get('attach_form'),
      '#description' => $this->t('Select to send form submissions to Sherpa CRM'),
      '#required' => FALSE,
      '#weight' => -20,
    ];

    $form['by_webform_setting'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Webform Settings'),
      '#states' => [
        'visible' => [
          ':input[name="attach_form"]' => ['checked' => TRUE]
        ],
      ],
    ];

    $form['by_webform_setting']['custom_use_quickset'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Quickset'),
      '#default_value' => null !== $custom_form_values->get('custom_use_quickset') ? $custom_form_values->get('custom_use_quickset') : $default_use_quickset,
      '#description' => $this->t('Sets required fields using default values that are normally entered as hidden fields (e.g. VendorName). Saves time and keeps the Webform nice and neat.'),
      '#required' => FALSE,
      '#weight' => -19,
    ];

    $form['by_webform_setting']['overwrite_community_id'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Custom Community ID'),
      '#default_value' => $custom_form_values->get('overwrite_community_id'),
      '#description' => $this->t('Set a custom Community ID for this Webform. You can change the Community ID below.'),
      '#required' => FALSE,
      '#weight' => -15,
    ];
		
    $form['by_webform_setting']['custom_community_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('This Webform\'s Community ID'),
      '#default_value' => $temp_community_id,
      '#description' => $this->t('This is the Community ID that this Webform will use.'),
      '#states' => [ 
        'enabled' => [
          ':input[name="overwrite_community_id"]' => [
            'checked' => TRUE,
          ],
        ],
        'required' => [
          ':input[name="overwrite_community_id"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
      '#required' => FALSE,
      '#weight' => -10,
    ];

    $form['by_webform_setting']['form_item'] = [
      '#markup' => '<br /><hr /><br />',
      '#weight' => -5,
    ];

    $form['by_webform_setting']['webform_container'] = [
      '#prefix' => "<div id=form-ajax-wrapper>",
      '#suffix' => "</div>",
    ];

    // Set counter to check if there are any supported fields and if not then display a message.
    $n = 0;

    foreach ($elements as $key => $element) {
      $selected_field = '_none';

      // Get the field type.
      $type = $element['#type'];
	  
	  // Get the multiple value. If it is TRUE then the field is set to allow multiple values.
	  $multiple = $element['#multiple'];

      // Make sure we do not display any unsupported field types or multiple value fields.
      if (in_array($type, $tested_fields) && !$multiple) {
		  
        $n++;
	
        if (!empty($default_values[$key])) {
          $selected_field = $default_values[$key]['sherpa_field'];
        }

        $form['by_webform_setting']['webform_container'][$key] = [
          '#type' => 'fieldset',
          '#title' => isset($element['#title']) ? $element['#title'] : $element['#type'],
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        ];

        $form['by_webform_setting']['webform_container'][$key][$key . '_sherpa_field'] = [
          '#type' => 'select',
          '#options' => $this->getSherpaFields(),
          '#default_value' => $selected_field,
          '#title' => t('Select Sherpa Data Element field'),
        ];

      }
	
    }

    if ($n === 0) {
      	\Drupal::messenger()->addWarning('This form does not contain any supported fields. Your form must contain at least one supported field to send data to emfluence. See help documentation for a list of supported fields.');
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];
		
    return $form;
		
  }

  /**
   * {@inheritdoc}
   */  	
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $storage = $form_state->getStorage();

    $data = [];

    foreach ($storage['elements'] as $key => $element) {
      $data[$key] = array(
        'sherpa_field' => $values[$key . '_sherpa_field'],
      );
    }

    $config = \Drupal::configFactory()->getEditable('sherpa_webform.webform_field_mapping.' . $storage['webformId']);
    $config->setData($data);

    $control = \Drupal::configFactory()->getEditable('sherpa_webform.webform_form_control.' . $storage['webformId']);
    $control->set('attach_form', $values['attach_form'])
      ->set('custom_use_quickset', $values['custom_use_quickset'])
      ->set('overwrite_community_id', $values['overwrite_community_id'])
      ->set('custom_community_id', $values['custom_community_id']);
    $config->save(TRUE);
    $control->save(TRUE);

    \Drupal::messenger()->addMessage(t('Fields mapping have been saved.'));
  }
	
  /**
   * Get prepared list of module fields.
   *
   * @return array
   *   Returns a list of module fields.
   */
  private function getSherpaFields() {
    // TODO: This list sucks. There must be a better way.
    // TODO: If we're including the entire list from Sherpa, then there needs to be some validation as some values are ints while some are strings
    $fields = array(
      '_none' => 'None',
      'VendorName' => 'VendorName',
      'SourceName' => 'SourceName',
      'ReferralType' => 'ReferralType',
      'AdvisorFirstName' => 'AdvisorFirstName',
      'AdvisorLastName' => 'AdvisorLastName',
      'AdvisorEmail' => 'AdvisorEmail',
      'LeadId' => 'LeadId',
      'VendorPropertyId' => 'VendorPropertyId',
      'PropertyName' => 'PropertyName',
      'PropertyPostalRegion' => 'PropertyPostalRegion',
      'AdvisorFax' => 'AdvisorFax',
      'AdvisorTollFree' => 'AdvisorTollFree',
      'AdvisorWorkPhone' => 'AdvisorWorkPhone',
      'DesiredZip' => 'DesiredZip',
      'AdvisorReferralNote' => 'AdvisorReferralNote',
      'TourDate' => 'TourDate',
      'TourDateAlternative' => 'TourDateAlternative',
      'PrimaryContactFirstName' => 'PrimaryContactFirstName',
      'PrimaryContactLastName' => 'PrimaryContactLastName',
      'PrimaryContactEmail' => 'PrimaryContactEmail',
      'PrimaryContactHomePhone' => 'PrimaryContactHomePhone',
      'PrimaryContactCellPhone' => 'PrimaryContactCellPhone',
      'PrimaryContactWorkPhone' => 'PrimaryContactWorkPhone',
      'PrimaryContactAddress1' => 'PrimaryContactAddress1',
      'PrimaryContactAddress2' => 'PrimaryContactAddress2',
      'PrimaryContactCity' => 'PrimaryContactCity',
      'PrimaryContactState' => 'PrimaryContactState',
      'PrimaryContactPostalCode' => 'PrimaryContactPostalCode',
      'PrimaryContactCountry' => 'PrimaryContactCountry',
      'PrimaryContactFaxPhone' => 'PrimaryContactFaxPhone',
      'PrimaryContactResidentRelationshipId' => 'PrimaryContactResidentRelationshipId',
      'PrimaryContactResidentRelationship' => 'PrimaryContactResidentRelationship',
      'ResidentContactFirstName' => 'ResidentContactFirstName',
      'ResidentContactLastName' => 'ResidentContactLastName',
      'ResidentContactHomePhone' => 'ResidentContactHomePhone',
      'ResidentContactCellPhone' => 'ResidentContactCellPhone',
      'ResidentContactWorkPhone' => 'ResidentContactWorkPhone',
      'ResidentContactAddress1' => 'ResidentContactAddress1',
      'ResidentContactAddress2' => 'ResidentContactAddress2',
      'ResidentContactCity' => 'ResidentContactCity',
      'ResidentContactState' => 'ResidentContactState',
      'ResidentContactPostalCode' => 'ResidentContactPostalCode',
      'ResidentContactCountry' => 'ResidentContactCountry',
      'ResidentContactFaxPhone' => 'ResidentContactFaxPhone',
      'ProfileAge' => 'ProfileAge',
      'ProfileActivityPreferences' => 'ProfileActivityPreferences',
      'ProfileAmbulationId' => 'ProfileAmbulationId',
      'ProfileAmbulation' => 'ProfileAmbulation',
      'ProfileAssistanceBathing' => 'ProfileAssistanceBathing',
      'ProfileAssistanceToileting' => 'ProfileAssistanceToileting',
      'ProfileBathroomReminderNeeded' => 'ProfileBathroomReminderNeeded',
      'ProfileBathroomPhysicalDifficulties' => 'ProfileBathroomPhysicalDifficulties',
      'ProfileDiabeticCareNeedsId' => 'ProfileDiabeticCareNeedsId',
      'ProfileDiabeticCareNeeds' => 'ProfileDiabeticCareNeeds',
      'ProfileDisability' => 'ProfileDisability',
      'ProfileFundingTypeId' => 'ProfileFundingTypeId',
      'ProfileFundingType' => 'ProfileFundingType',
      'ProfileAlternateFundingType' => 'ProfileAlternateFundingType',
      'ProfileGenderCode' => 'ProfileGenderCode',
      'ProfileGender' => 'ProfileGender',
      'ProfileIncontinenceTypeBladder' => 'ProfileIncontinenceTypeBladder',
      'ProfileIncontinenceTypeBowel' => 'ProfileIncontinenceTypeBowel',
      'ProfileLengthCareNeededId' => 'ProfileLengthCareNeededId',
      'ProfileLengthCareNeeded' => 'ProfileLengthCareNeeded',
      'ProfileBudgetId' => 'ProfileBudgetId',
      'ProfileBudget' => 'ProfileBudget',
      'ProfileMedicalHistory' => 'ProfileMedicalHistory',
      'ProfileMedicalDiagnoses' => 'ProfileMedicalDiagnoses',
      'ProfileMedication' => 'ProfileMedication',
      'ProfileMemoryCombativeId' => 'ProfileMemoryCombativeId',
      'ProfileMemoryCombative' => 'ProfileMemoryCombative',
      'ProfileMemoryDiagnosisId' => 'ProfileMemoryDiagnosisId',
      'ProfileMemoryDiagnosis' => 'ProfileMemoryDiagnosis',
      'ProfileMemoryLossId' => 'ProfileMemoryLossId',
      'ProfileMemoryLoss' => 'ProfileMemoryLoss',
      'ProfileOtherAccommodation' => 'ProfileOtherAccommodation',
      'ProfileOtherAssistance' => 'ProfileOtherAssistance',
      'ProfileOtherHealthIssues' => 'ProfileOtherHealthIssues',
      'ProfileOtherInfo' => 'ProfileOtherInfo',
      'ProfileOtherLivingSituation' => 'ProfileOtherLivingSituation',
      'ProfileOtherPreferences' => 'ProfileOtherPreferences',
      'ProfileReasonsForSearch' => 'ProfileReasonsForSearch',
      'ProfileLivingSituationId' => 'ProfileLivingSituationId',
      'ProfileLivingSituation' => 'ProfileLivingSituation',
      'ProfileMaritalStatusCode' => 'ProfileMaritalStatusCode',
      'ProfileTakingMedicationId' => 'ProfileTakingMedicationId',
      'ProfileVeteran' => 'ProfileVeteran',
      'Tactic' => 'Tactic',
      'CampaignID' => 'CampaignID',
      'Campaign' => 'Campaign',
      'Subtactic' => 'Subtactic',
      'Rsvp' => 'Rsvp',
    );
    return $fields;
  }	
	
}
