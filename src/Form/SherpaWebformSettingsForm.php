<?php

namespace Drupal\sherpa_webform\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\Entity\DateFormat;

/**
 * Class SherpaWebformSettingsForm
 *
 * @package Drupal\sherpa_webform\Form
 */
class SherpaWebformSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sherpa_webform_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sherpa_webform.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Identify if curl or restful services are available & construct a message on what is and is not available to the module
    $moduleHandler = \Drupal::service('module_handler');
    $message = '<ul><li>';

    // Assume that neither curl nor restful services are available
    $no_drupal_rest = TRUE;
    $no_curl = TRUE;
    if ($moduleHandler->moduleExists('rest')){
      $message .= 'The RESTful Web Services module is enabled. You can use RESTful Web Services.<br />';
      $no_drupal_rest = FALSE;
    } else {
      $message .= 'The RESTful Web Services module is disabled.<br />';
    }

    $message .= '</li><li>';

    if (function_exists('curl_version')) {
      $message .= 'cURL has been detected.<br />';
      $no_curl = FALSE;
    } else {
      $message .= 'cURL has not been detected.<br />';
    }

    $message .= '</li></ul>';

    if ($no_drupal_rest && $no_curl) {
      $message .= '<p><strong>Sherpa Webform cannot detect a method to send data to Sherpa.</strong><br />';
      $message .= '<span style="color: #ff0000">You need either the RESTful Web Services module enabled or cURL installed and enabled on your server.</span></p>';
    } elseif (!$no_drupal_rest && $no_curl) {
      $message .= '<p><span style="color: #ff0000">Make sure you have the Use RESTful Web Services checkbox checked, below.</span></p>';
    }		

    // Get the date types that are available for selection in the form
    $date_types = DateFormat::loadMultiple();
    $date_formatter = \Drupal::service('date.formatter');
    foreach ($date_types as $machine_name => $format) {
      $date_formats[$machine_name] = $date_formatter->format(REQUEST_TIME, $machine_name);
    }

    $sherpa_webform_config = $this->config('sherpa_webform.settings');		

    $form['company_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company ID'),
      '#default_value' => $sherpa_webform_config->get('company_id'),
      '#size' => 64,
      '#maxlength' => 64,
      '#description' => $this->t('The Company ID'),
      '#required' => TRUE,
    ];

    $form['community_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Community ID'),
      '#default_value' => $sherpa_webform_config->get('community_id'),
      '#size' => 64,
      '#maxlength' => 64,
      '#description' => $this->t('The Community ID'),
      '#required' => TRUE,
    ];

    $form['base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sherpa API URL'),
      '#default_value' => $sherpa_webform_config->get('base_url'),
      '#size' => 64,
      '#maxlength' => 128,
      '#description' => $this->t('The URL to the Sherpa CRM web service (including https://)'),
      '#required' => TRUE,
    ];

    $form['enable_debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Debugging'),
      '#default_value' => $sherpa_webform_config->get('enable_debug'),
      '#description' => $this->t('Enable debugging. If disabled, only the POST / Response will be sent to the Drupal log.'),
      '#required' => FALSE,
    ];

    $form['integration'] = [
      '#type' => 'fieldset',
      '#title' => 'Integration Options',
    ];

    $form['integration']['spit_it_out'] = [
      '#markup' => $message,
    ];

    $form['integration']['use_drupal'] = [
      '#type' => 'checkbox',
      '#title'=> $this->t('Use RESTful Web Services'),
      '#default_value' => ($no_drupal_rest ? 0 : $sherpa_webform_config->get('use_drupal')),
      '#description' => $this->t('Use Drupal\'s RESTful Web Services module.') . ($no_drupal_rest ? $this->t(' Module not enabled') : ''),
      '#disabled' => $no_drupal_rest,
    ];

    $form['integration']['use_quickset'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Quickset'),
      '#default_value' => $sherpa_webform_config->get('use_quickset'),
      '#description' => $this->t('Sets required fields using default values that are normally entered as hidden fields (e.g. VendorName). Saves time and keeps the Webform nice and neat. Please note: This will not reset the Use Quickset settings in any configured Webforms.'),
      '#required' => FALSE,
    ];

    $form['date_time'] = [
      '#type' => 'fieldset',
      '#title' => 'Sherpa Date &amp; Time Fields'
    ];

    $form['date_time']['time_title'] = [
      '#markup' => '<p>You can set custom values for ReferralDate and ReferralDateTime using Drupal\'s Date and time formats. Defaults have been provided.</p>',
    ];

    $form['date_time']['sherpa_referral_date'] = [
      '#type' => 'select',
      '#title' => $this->t('Sherpa ReferralDate'),
      '#description' => t('Used for ReferralDate field. Sherpa default is ') . $date_formatter->format(REQUEST_TIME, 'sherpa_referral_date') .  t(' (Y-m-d).'),
      '#options' => $date_formats,
      '#default_value' => $sherpa_webform_config->get('sherpa_referral_date'),
    ];

    $form['date_time']['sherpa_referral_date_time'] = [
      '#type' => 'select',
      '#title' => $this->t('Sherpa ReferralDateTime'),
      '#description' => t('Used for ReferralDateTime field. Sherpa default is ') . $date_formatter->format(REQUEST_TIME, 'sherpa_referral_date_time') . t(' (Y-m-d\zH:i:s).'),
      '#options' => $date_formats,
      '#default_value' => $sherpa_webform_config->get('sherpa_referral_date_time'),
    ];

    return parent::buildForm($form, $form_state);
	
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */  
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();

    $this->config('sherpa_webform.settings')
      ->set('company_id', $values['company_id'])
      ->set('community_id', $values['community_id'])
      ->set('base_url', $values['base_url'])
      ->set('use_quickset', $values['use_quickset'])
      ->set('enable_debug', $values['enable_debug'])
      ->set('use_drupal', $values['use_drupal'])
      ->set('sherpa_referral_date', $values['sherpa_referral_date'])
      ->set('sherpa_referral_date_time', $values['sherpa_referral_date_time'])
      ->save();

  }
}
