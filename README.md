# Sherpa Webform

## Installation

Install like any other Drupal module.

## Configuration

* Global settings for the module are found on the Sherpa Webform configuration page. The default values provided should be enough to begin testing in Sherpa's sandbox environment. To move to production you will need a Production URL, Company ID and Community ID provided by Sherpa.
* Webforms are configured individually from the Build menu under the Sherpa Webform Field Mapping tab. You can enable the form to send to Sherpa, set a custom Community ID and map the fields to their Sherpa Data Element value.
* For more information, see the module's help documentation page.

## Version Information

1.1:
- Roll-up of 1.x-dev[23AUG19], 1.x-dev[16AUG19], & 1.x-dev[11AUG19]

1.x-dev[23AUG19]:
- limited webform field types that can be mapped & built a mamager class to keep track of them
- filter out fields with multiple values (fileds with a number of values set to anything greater than 1) from field mapping
- updated help docs to reflect limiting of webform fields
- added check and alert to field mapping if there are no supported fields in webform
- updated field mapping form id to sherpa_webform_field_mapping
- removed field restriction from SherpaWebformFieldsMapping submitForm()
- updated code layout to conform to standards
- updated description in info & composer files
- commented out un-used code

1.x-dev[16AUG19]:
- Updated readme doc
- Update help documentation
- Update module description in info.yml
- Update debug message on initial webform submission

1.x-dev[11AUG19]:
- remove version number from info file (do'h)
- remove redundent variable assingment from SherpaWebformFieldsMapping
- correct description of quick set for item in SherpaWebformFieldsMapping

1.0:
- Production release

0.8-beta:
- Added composer.json

0.7-beta:
- Added comments and formatted "the Drupal way"
- Changed some form field code for consistancy

0.6-beta:
- Cleaned up the admin page using fieldsets
- Cleaned up webform config page by hiding options when form is not selected to be sent to sherpa
- Finished Quick Set for use on a form by form basis (with global value)

0.5-beta:
- Added Sherpa's default ReferralDate & ReferralDateTime to Drupal's Date Formats
- Allow users to set ReferralDate & ReferralDateTime from admin page

0.4.1-beta:
- fixed 'Undefined index: actions_sherpa_field' issue
- work begun on advanced quickset options

0.4-beta:
- update readme
- update Webform settings form (conditional validation) & removed 'actions' buttons from mapping list
- Removed un-used JS and libraries.yml 

0.3-beta:
- Removed API Key from configuration & cURL header
- Changed the label name of Base URL to Sherpa API URL on the configuration screen
- Changed the page name of Sherpa Webform Field Mapping to Sherpa Configuration
- Added the ability to use Drupal's RESTful Web Services module
- Added additional entries logs for verbose logging setting
- Fixed spelling and grammar issues in the Help documents

0.2-beta:
- Added Quick Set function
- Added interface for individual Webform integration under Build menu
- Changed Webform selection from comma seperated list in main configuration to individual Webform checkbox
- Added field mapping and custom Community ID
- Added module help page and readme document
- Added framework for future scripting of interface elements under Sherpa Webform Field Mapping
- Added version number (I need to keep track with all these sites)

0.1-beta:
- Basic functionality with code mapped fields. Very primitive.

Things to do:
- Disable sending if all form fields are set without mapping even if send form to sherpa selected
- Allow selection of a data element limited to a single instance (ajax remove them as you go)
	- remove them if quickset is checked for the form
- Get correct sandbox company & community id for default values
- Allow for selection of v1 or v2 of sherpa and adjust config fields based on version (create a v2 of module?)
	- need correct sandbox values
- is webform_submission_presave() correct or should it be hook_webform_submission_actions($node, $submission) https://sites.google.com/site/ravivmane/webform-in-drupal/webform-hooks-example
- test and build for composit elements in field mapping. try to find a way to filter out unwanted elements (like captcha or file upload elements).
- solution for using a select field to define community id
- end user editing (globally) of quick set values
